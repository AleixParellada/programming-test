// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EndlingTest/RabbitAIController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRabbitAIController() {}
// Cross Module References
	ENDLINGTEST_API UClass* Z_Construct_UClass_ARabbitAIController_NoRegister();
	ENDLINGTEST_API UClass* Z_Construct_UClass_ARabbitAIController();
	AIMODULE_API UClass* Z_Construct_UClass_AAIController();
	UPackage* Z_Construct_UPackage__Script_EndlingTest();
	ENDLINGTEST_API UFunction* Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveExecute();
	ENDLINGTEST_API UFunction* Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveTick();
	ENDLINGTEST_API UFunction* Z_Construct_UFunction_ARabbitAIController_OnOverlapBegin();
	ENDLINGTEST_API UFunction* Z_Construct_UFunction_ARabbitAIController_OnPawnDied();
	ENDLINGTEST_API UFunction* Z_Construct_UFunction_ARabbitAIController_OnPerceptionUpdated();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	AIMODULE_API UClass* Z_Construct_UClass_UBlackboardComponent_NoRegister();
// End Cross Module References
	void ARabbitAIController::StaticRegisterNativesARabbitAIController()
	{
		UClass* Class = ARabbitAIController::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnFleeReceiveExecute", &ARabbitAIController::execOnFleeReceiveExecute },
			{ "OnFleeReceiveTick", &ARabbitAIController::execOnFleeReceiveTick },
			{ "OnOverlapBegin", &ARabbitAIController::execOnOverlapBegin },
			{ "OnPawnDied", &ARabbitAIController::execOnPawnDied },
			{ "OnPerceptionUpdated", &ARabbitAIController::execOnPerceptionUpdated },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveExecute_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveExecute_Statics::Function_MetaDataParams[] = {
		{ "Category", "CppFunctionCall" },
		{ "ModuleRelativePath", "RabbitAIController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveExecute_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARabbitAIController, "OnFleeReceiveExecute", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04040401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveExecute_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveExecute_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveExecute()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveExecute_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveTick_Statics
	{
		struct RabbitAIController_eventOnFleeReceiveTick_Parms
		{
			float DeltaTime;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DeltaTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveTick_Statics::NewProp_DeltaTime = { UE4CodeGen_Private::EPropertyClass::Float, "DeltaTime", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(RabbitAIController_eventOnFleeReceiveTick_Parms, DeltaTime), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveTick_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveTick_Statics::NewProp_DeltaTime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveTick_Statics::Function_MetaDataParams[] = {
		{ "Category", "CppFunctionCall" },
		{ "ModuleRelativePath", "RabbitAIController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveTick_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARabbitAIController, "OnFleeReceiveTick", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04040401, sizeof(RabbitAIController_eventOnFleeReceiveTick_Parms), Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveTick_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveTick_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveTick_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveTick_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveTick()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveTick_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARabbitAIController_OnOverlapBegin_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARabbitAIController_OnOverlapBegin_Statics::Function_MetaDataParams[] = {
		{ "Category", "CppFunctionCall" },
		{ "ModuleRelativePath", "RabbitAIController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARabbitAIController_OnOverlapBegin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARabbitAIController, "OnOverlapBegin", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04040401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARabbitAIController_OnOverlapBegin_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARabbitAIController_OnOverlapBegin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARabbitAIController_OnOverlapBegin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARabbitAIController_OnOverlapBegin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARabbitAIController_OnPawnDied_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARabbitAIController_OnPawnDied_Statics::Function_MetaDataParams[] = {
		{ "Category", "CppFunctionCall" },
		{ "ModuleRelativePath", "RabbitAIController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARabbitAIController_OnPawnDied_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARabbitAIController, "OnPawnDied", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04040401, 0, nullptr, 0, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARabbitAIController_OnPawnDied_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARabbitAIController_OnPawnDied_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARabbitAIController_OnPawnDied()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARabbitAIController_OnPawnDied_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ARabbitAIController_OnPerceptionUpdated_Statics
	{
		struct RabbitAIController_eventOnPerceptionUpdated_Parms
		{
			TArray<AActor*> UpdatedActors;
		};
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_UpdatedActors;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_UpdatedActors_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ARabbitAIController_OnPerceptionUpdated_Statics::NewProp_UpdatedActors = { UE4CodeGen_Private::EPropertyClass::Array, "UpdatedActors", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(RabbitAIController_eventOnPerceptionUpdated_Parms, UpdatedActors), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ARabbitAIController_OnPerceptionUpdated_Statics::NewProp_UpdatedActors_Inner = { UE4CodeGen_Private::EPropertyClass::Object, "UpdatedActors", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0000000000000000, 1, nullptr, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ARabbitAIController_OnPerceptionUpdated_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARabbitAIController_OnPerceptionUpdated_Statics::NewProp_UpdatedActors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ARabbitAIController_OnPerceptionUpdated_Statics::NewProp_UpdatedActors_Inner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ARabbitAIController_OnPerceptionUpdated_Statics::Function_MetaDataParams[] = {
		{ "Category", "CppFunctionCall" },
		{ "ModuleRelativePath", "RabbitAIController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ARabbitAIController_OnPerceptionUpdated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ARabbitAIController, "OnPerceptionUpdated", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04040401, sizeof(RabbitAIController_eventOnPerceptionUpdated_Parms), Z_Construct_UFunction_ARabbitAIController_OnPerceptionUpdated_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_ARabbitAIController_OnPerceptionUpdated_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ARabbitAIController_OnPerceptionUpdated_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_ARabbitAIController_OnPerceptionUpdated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ARabbitAIController_OnPerceptionUpdated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ARabbitAIController_OnPerceptionUpdated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ARabbitAIController_NoRegister()
	{
		return ARabbitAIController::StaticClass();
	}
	struct Z_Construct_UClass_ARabbitAIController_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FleeSpeed_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FleeSpeed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BlackboardComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BlackboardComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ARabbitAIController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AAIController,
		(UObject* (*)())Z_Construct_UPackage__Script_EndlingTest,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ARabbitAIController_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveExecute, "OnFleeReceiveExecute" }, // 725633970
		{ &Z_Construct_UFunction_ARabbitAIController_OnFleeReceiveTick, "OnFleeReceiveTick" }, // 2461544135
		{ &Z_Construct_UFunction_ARabbitAIController_OnOverlapBegin, "OnOverlapBegin" }, // 3721965434
		{ &Z_Construct_UFunction_ARabbitAIController_OnPawnDied, "OnPawnDied" }, // 1886957612
		{ &Z_Construct_UFunction_ARabbitAIController_OnPerceptionUpdated, "OnPerceptionUpdated" }, // 1216021472
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARabbitAIController_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "RabbitAIController.h" },
		{ "ModuleRelativePath", "RabbitAIController.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARabbitAIController_Statics::NewProp_FleeSpeed_MetaData[] = {
		{ "Category", "CppVariable" },
		{ "ModuleRelativePath", "RabbitAIController.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ARabbitAIController_Statics::NewProp_FleeSpeed = { UE4CodeGen_Private::EPropertyClass::Float, "FleeSpeed", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, STRUCT_OFFSET(ARabbitAIController, FleeSpeed), METADATA_PARAMS(Z_Construct_UClass_ARabbitAIController_Statics::NewProp_FleeSpeed_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARabbitAIController_Statics::NewProp_FleeSpeed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARabbitAIController_Statics::NewProp_BlackboardComponent_MetaData[] = {
		{ "Category", "CppVariable" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "RabbitAIController.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ARabbitAIController_Statics::NewProp_BlackboardComponent = { UE4CodeGen_Private::EPropertyClass::Object, "BlackboardComponent", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x001000000008000d, 1, nullptr, STRUCT_OFFSET(ARabbitAIController, BlackboardComponent), Z_Construct_UClass_UBlackboardComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ARabbitAIController_Statics::NewProp_BlackboardComponent_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARabbitAIController_Statics::NewProp_BlackboardComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ARabbitAIController_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARabbitAIController_Statics::NewProp_FleeSpeed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARabbitAIController_Statics::NewProp_BlackboardComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ARabbitAIController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ARabbitAIController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ARabbitAIController_Statics::ClassParams = {
		&ARabbitAIController::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009002A0u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		Z_Construct_UClass_ARabbitAIController_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ARabbitAIController_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ARabbitAIController_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ARabbitAIController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ARabbitAIController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ARabbitAIController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ARabbitAIController, 1847169700);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ARabbitAIController(Z_Construct_UClass_ARabbitAIController, &ARabbitAIController::StaticClass, TEXT("/Script/EndlingTest"), TEXT("ARabbitAIController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARabbitAIController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
