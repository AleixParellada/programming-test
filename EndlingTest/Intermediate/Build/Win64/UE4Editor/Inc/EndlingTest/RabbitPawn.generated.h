// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ENDLINGTEST_RabbitPawn_generated_h
#error "RabbitPawn.generated.h already included, missing '#pragma once' in RabbitPawn.h"
#endif
#define ENDLINGTEST_RabbitPawn_generated_h

#define EndlingTest_Source_EndlingTest_RabbitPawn_h_10_RPC_WRAPPERS
#define EndlingTest_Source_EndlingTest_RabbitPawn_h_10_RPC_WRAPPERS_NO_PURE_DECLS
#define EndlingTest_Source_EndlingTest_RabbitPawn_h_10_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARabbitPawn(); \
	friend struct Z_Construct_UClass_ARabbitPawn_Statics; \
public: \
	DECLARE_CLASS(ARabbitPawn, APawn, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EndlingTest"), NO_API) \
	DECLARE_SERIALIZER(ARabbitPawn)


#define EndlingTest_Source_EndlingTest_RabbitPawn_h_10_INCLASS \
private: \
	static void StaticRegisterNativesARabbitPawn(); \
	friend struct Z_Construct_UClass_ARabbitPawn_Statics; \
public: \
	DECLARE_CLASS(ARabbitPawn, APawn, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EndlingTest"), NO_API) \
	DECLARE_SERIALIZER(ARabbitPawn)


#define EndlingTest_Source_EndlingTest_RabbitPawn_h_10_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARabbitPawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARabbitPawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARabbitPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARabbitPawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARabbitPawn(ARabbitPawn&&); \
	NO_API ARabbitPawn(const ARabbitPawn&); \
public:


#define EndlingTest_Source_EndlingTest_RabbitPawn_h_10_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARabbitPawn(ARabbitPawn&&); \
	NO_API ARabbitPawn(const ARabbitPawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARabbitPawn); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARabbitPawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ARabbitPawn)


#define EndlingTest_Source_EndlingTest_RabbitPawn_h_10_PRIVATE_PROPERTY_OFFSET
#define EndlingTest_Source_EndlingTest_RabbitPawn_h_7_PROLOG
#define EndlingTest_Source_EndlingTest_RabbitPawn_h_10_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlingTest_Source_EndlingTest_RabbitPawn_h_10_PRIVATE_PROPERTY_OFFSET \
	EndlingTest_Source_EndlingTest_RabbitPawn_h_10_RPC_WRAPPERS \
	EndlingTest_Source_EndlingTest_RabbitPawn_h_10_INCLASS \
	EndlingTest_Source_EndlingTest_RabbitPawn_h_10_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlingTest_Source_EndlingTest_RabbitPawn_h_10_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlingTest_Source_EndlingTest_RabbitPawn_h_10_PRIVATE_PROPERTY_OFFSET \
	EndlingTest_Source_EndlingTest_RabbitPawn_h_10_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlingTest_Source_EndlingTest_RabbitPawn_h_10_INCLASS_NO_PURE_DECLS \
	EndlingTest_Source_EndlingTest_RabbitPawn_h_10_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlingTest_Source_EndlingTest_RabbitPawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
