// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef ENDLINGTEST_RabbitAIController_generated_h
#error "RabbitAIController.generated.h already included, missing '#pragma once' in RabbitAIController.h"
#endif
#define ENDLINGTEST_RabbitAIController_generated_h

#define EndlingTest_Source_EndlingTest_RabbitAIController_h_16_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnPawnDied) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnPawnDied(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnFleeReceiveTick) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_DeltaTime); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnFleeReceiveTick(Z_Param_DeltaTime); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnFleeReceiveExecute) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnFleeReceiveExecute(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnOverlapBegin) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapBegin(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnPerceptionUpdated) \
	{ \
		P_GET_TARRAY(AActor*,Z_Param_UpdatedActors); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnPerceptionUpdated(Z_Param_UpdatedActors); \
		P_NATIVE_END; \
	}


#define EndlingTest_Source_EndlingTest_RabbitAIController_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnPawnDied) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnPawnDied(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnFleeReceiveTick) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_DeltaTime); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnFleeReceiveTick(Z_Param_DeltaTime); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnFleeReceiveExecute) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnFleeReceiveExecute(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnOverlapBegin) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlapBegin(); \
		P_NATIVE_END; \
	} \
 \
	DECLARE_FUNCTION(execOnPerceptionUpdated) \
	{ \
		P_GET_TARRAY(AActor*,Z_Param_UpdatedActors); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnPerceptionUpdated(Z_Param_UpdatedActors); \
		P_NATIVE_END; \
	}


#define EndlingTest_Source_EndlingTest_RabbitAIController_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesARabbitAIController(); \
	friend struct Z_Construct_UClass_ARabbitAIController_Statics; \
public: \
	DECLARE_CLASS(ARabbitAIController, AAIController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EndlingTest"), NO_API) \
	DECLARE_SERIALIZER(ARabbitAIController)


#define EndlingTest_Source_EndlingTest_RabbitAIController_h_16_INCLASS \
private: \
	static void StaticRegisterNativesARabbitAIController(); \
	friend struct Z_Construct_UClass_ARabbitAIController_Statics; \
public: \
	DECLARE_CLASS(ARabbitAIController, AAIController, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/EndlingTest"), NO_API) \
	DECLARE_SERIALIZER(ARabbitAIController)


#define EndlingTest_Source_EndlingTest_RabbitAIController_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARabbitAIController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARabbitAIController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARabbitAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARabbitAIController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARabbitAIController(ARabbitAIController&&); \
	NO_API ARabbitAIController(const ARabbitAIController&); \
public:


#define EndlingTest_Source_EndlingTest_RabbitAIController_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ARabbitAIController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ARabbitAIController(ARabbitAIController&&); \
	NO_API ARabbitAIController(const ARabbitAIController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ARabbitAIController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ARabbitAIController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ARabbitAIController)


#define EndlingTest_Source_EndlingTest_RabbitAIController_h_16_PRIVATE_PROPERTY_OFFSET
#define EndlingTest_Source_EndlingTest_RabbitAIController_h_13_PROLOG
#define EndlingTest_Source_EndlingTest_RabbitAIController_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlingTest_Source_EndlingTest_RabbitAIController_h_16_PRIVATE_PROPERTY_OFFSET \
	EndlingTest_Source_EndlingTest_RabbitAIController_h_16_RPC_WRAPPERS \
	EndlingTest_Source_EndlingTest_RabbitAIController_h_16_INCLASS \
	EndlingTest_Source_EndlingTest_RabbitAIController_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlingTest_Source_EndlingTest_RabbitAIController_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlingTest_Source_EndlingTest_RabbitAIController_h_16_PRIVATE_PROPERTY_OFFSET \
	EndlingTest_Source_EndlingTest_RabbitAIController_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlingTest_Source_EndlingTest_RabbitAIController_h_16_INCLASS_NO_PURE_DECLS \
	EndlingTest_Source_EndlingTest_RabbitAIController_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlingTest_Source_EndlingTest_RabbitAIController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
