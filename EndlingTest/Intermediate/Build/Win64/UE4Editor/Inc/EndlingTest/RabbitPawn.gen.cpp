// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EndlingTest/RabbitPawn.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeRabbitPawn() {}
// Cross Module References
	ENDLINGTEST_API UClass* Z_Construct_UClass_ARabbitPawn_NoRegister();
	ENDLINGTEST_API UClass* Z_Construct_UClass_ARabbitPawn();
	ENGINE_API UClass* Z_Construct_UClass_APawn();
	UPackage* Z_Construct_UPackage__Script_EndlingTest();
// End Cross Module References
	void ARabbitPawn::StaticRegisterNativesARabbitPawn()
	{
	}
	UClass* Z_Construct_UClass_ARabbitPawn_NoRegister()
	{
		return ARabbitPawn::StaticClass();
	}
	struct Z_Construct_UClass_ARabbitPawn_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HasOverlappedCharacter_MetaData[];
#endif
		static void NewProp_HasOverlappedCharacter_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_HasOverlappedCharacter;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ARabbitPawn_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APawn,
		(UObject* (*)())Z_Construct_UPackage__Script_EndlingTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARabbitPawn_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "RabbitPawn.h" },
		{ "ModuleRelativePath", "RabbitPawn.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ARabbitPawn_Statics::NewProp_HasOverlappedCharacter_MetaData[] = {
		{ "Category", "CppVariable" },
		{ "ModuleRelativePath", "RabbitPawn.h" },
	};
#endif
	void Z_Construct_UClass_ARabbitPawn_Statics::NewProp_HasOverlappedCharacter_SetBit(void* Obj)
	{
		((ARabbitPawn*)Obj)->HasOverlappedCharacter = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ARabbitPawn_Statics::NewProp_HasOverlappedCharacter = { UE4CodeGen_Private::EPropertyClass::Bool, "HasOverlappedCharacter", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000005, 1, nullptr, sizeof(bool), UE4CodeGen_Private::ENativeBool::Native, sizeof(ARabbitPawn), &Z_Construct_UClass_ARabbitPawn_Statics::NewProp_HasOverlappedCharacter_SetBit, METADATA_PARAMS(Z_Construct_UClass_ARabbitPawn_Statics::NewProp_HasOverlappedCharacter_MetaData, ARRAY_COUNT(Z_Construct_UClass_ARabbitPawn_Statics::NewProp_HasOverlappedCharacter_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ARabbitPawn_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ARabbitPawn_Statics::NewProp_HasOverlappedCharacter,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ARabbitPawn_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ARabbitPawn>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ARabbitPawn_Statics::ClassParams = {
		&ARabbitPawn::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009000A0u,
		nullptr, 0,
		Z_Construct_UClass_ARabbitPawn_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UClass_ARabbitPawn_Statics::PropPointers),
		nullptr,
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_ARabbitPawn_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_ARabbitPawn_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ARabbitPawn()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ARabbitPawn_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ARabbitPawn, 2441003307);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ARabbitPawn(Z_Construct_UClass_ARabbitPawn, &ARabbitPawn::StaticClass, TEXT("/Script/EndlingTest"), TEXT("ARabbitPawn"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ARabbitPawn);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
