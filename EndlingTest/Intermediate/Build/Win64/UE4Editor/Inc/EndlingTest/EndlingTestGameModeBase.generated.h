// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ENDLINGTEST_EndlingTestGameModeBase_generated_h
#error "EndlingTestGameModeBase.generated.h already included, missing '#pragma once' in EndlingTestGameModeBase.h"
#endif
#define ENDLINGTEST_EndlingTestGameModeBase_generated_h

#define EndlingTest_Source_EndlingTest_EndlingTestGameModeBase_h_15_RPC_WRAPPERS
#define EndlingTest_Source_EndlingTest_EndlingTestGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define EndlingTest_Source_EndlingTest_EndlingTestGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEndlingTestGameModeBase(); \
	friend struct Z_Construct_UClass_AEndlingTestGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AEndlingTestGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/EndlingTest"), NO_API) \
	DECLARE_SERIALIZER(AEndlingTestGameModeBase)


#define EndlingTest_Source_EndlingTest_EndlingTestGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAEndlingTestGameModeBase(); \
	friend struct Z_Construct_UClass_AEndlingTestGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AEndlingTestGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/EndlingTest"), NO_API) \
	DECLARE_SERIALIZER(AEndlingTestGameModeBase)


#define EndlingTest_Source_EndlingTest_EndlingTestGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEndlingTestGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEndlingTestGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEndlingTestGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEndlingTestGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEndlingTestGameModeBase(AEndlingTestGameModeBase&&); \
	NO_API AEndlingTestGameModeBase(const AEndlingTestGameModeBase&); \
public:


#define EndlingTest_Source_EndlingTest_EndlingTestGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEndlingTestGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEndlingTestGameModeBase(AEndlingTestGameModeBase&&); \
	NO_API AEndlingTestGameModeBase(const AEndlingTestGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEndlingTestGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEndlingTestGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEndlingTestGameModeBase)


#define EndlingTest_Source_EndlingTest_EndlingTestGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define EndlingTest_Source_EndlingTest_EndlingTestGameModeBase_h_12_PROLOG
#define EndlingTest_Source_EndlingTest_EndlingTestGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlingTest_Source_EndlingTest_EndlingTestGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	EndlingTest_Source_EndlingTest_EndlingTestGameModeBase_h_15_RPC_WRAPPERS \
	EndlingTest_Source_EndlingTest_EndlingTestGameModeBase_h_15_INCLASS \
	EndlingTest_Source_EndlingTest_EndlingTestGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlingTest_Source_EndlingTest_EndlingTestGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlingTest_Source_EndlingTest_EndlingTestGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	EndlingTest_Source_EndlingTest_EndlingTestGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlingTest_Source_EndlingTest_EndlingTestGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	EndlingTest_Source_EndlingTest_EndlingTestGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlingTest_Source_EndlingTest_EndlingTestGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
