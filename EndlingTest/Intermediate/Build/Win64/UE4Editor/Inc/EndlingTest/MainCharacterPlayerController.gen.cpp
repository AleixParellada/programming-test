// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EndlingTest/MainCharacterPlayerController.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMainCharacterPlayerController() {}
// Cross Module References
	ENDLINGTEST_API UClass* Z_Construct_UClass_AMainCharacterPlayerController_NoRegister();
	ENDLINGTEST_API UClass* Z_Construct_UClass_AMainCharacterPlayerController();
	ENGINE_API UClass* Z_Construct_UClass_APlayerController();
	UPackage* Z_Construct_UPackage__Script_EndlingTest();
	ENDLINGTEST_API UFunction* Z_Construct_UFunction_AMainCharacterPlayerController_MovementInput();
// End Cross Module References
	void AMainCharacterPlayerController::StaticRegisterNativesAMainCharacterPlayerController()
	{
		UClass* Class = AMainCharacterPlayerController::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "MovementInput", &AMainCharacterPlayerController::execMovementInput },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AMainCharacterPlayerController_MovementInput_Statics
	{
		struct MainCharacterPlayerController_eventMovementInput_Parms
		{
			float AxisValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_AxisValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AMainCharacterPlayerController_MovementInput_Statics::NewProp_AxisValue = { UE4CodeGen_Private::EPropertyClass::Float, "AxisValue", RF_Public|RF_Transient|RF_MarkAsNative, (EPropertyFlags)0x0010000000000080, 1, nullptr, STRUCT_OFFSET(MainCharacterPlayerController_eventMovementInput_Parms, AxisValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AMainCharacterPlayerController_MovementInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AMainCharacterPlayerController_MovementInput_Statics::NewProp_AxisValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AMainCharacterPlayerController_MovementInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "CppFunctionCall" },
		{ "ModuleRelativePath", "MainCharacterPlayerController.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AMainCharacterPlayerController_MovementInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AMainCharacterPlayerController, "MovementInput", RF_Public|RF_Transient|RF_MarkAsNative, nullptr, (EFunctionFlags)0x04040401, sizeof(MainCharacterPlayerController_eventMovementInput_Parms), Z_Construct_UFunction_AMainCharacterPlayerController_MovementInput_Statics::PropPointers, ARRAY_COUNT(Z_Construct_UFunction_AMainCharacterPlayerController_MovementInput_Statics::PropPointers), 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AMainCharacterPlayerController_MovementInput_Statics::Function_MetaDataParams, ARRAY_COUNT(Z_Construct_UFunction_AMainCharacterPlayerController_MovementInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AMainCharacterPlayerController_MovementInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AMainCharacterPlayerController_MovementInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AMainCharacterPlayerController_NoRegister()
	{
		return AMainCharacterPlayerController::StaticClass();
	}
	struct Z_Construct_UClass_AMainCharacterPlayerController_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AMainCharacterPlayerController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_APlayerController,
		(UObject* (*)())Z_Construct_UPackage__Script_EndlingTest,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AMainCharacterPlayerController_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AMainCharacterPlayerController_MovementInput, "MovementInput" }, // 1122261189
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AMainCharacterPlayerController_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "MainCharacterPlayerController.h" },
		{ "ModuleRelativePath", "MainCharacterPlayerController.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AMainCharacterPlayerController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AMainCharacterPlayerController>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AMainCharacterPlayerController_Statics::ClassParams = {
		&AMainCharacterPlayerController::StaticClass,
		DependentSingletons, ARRAY_COUNT(DependentSingletons),
		0x009002A4u,
		FuncInfo, ARRAY_COUNT(FuncInfo),
		nullptr, 0,
		"Game",
		&StaticCppClassTypeInfo,
		nullptr, 0,
		METADATA_PARAMS(Z_Construct_UClass_AMainCharacterPlayerController_Statics::Class_MetaDataParams, ARRAY_COUNT(Z_Construct_UClass_AMainCharacterPlayerController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AMainCharacterPlayerController()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AMainCharacterPlayerController_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AMainCharacterPlayerController, 1710983722);
	static FCompiledInDefer Z_CompiledInDefer_UClass_AMainCharacterPlayerController(Z_Construct_UClass_AMainCharacterPlayerController, &AMainCharacterPlayerController::StaticClass, TEXT("/Script/EndlingTest"), TEXT("AMainCharacterPlayerController"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AMainCharacterPlayerController);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
