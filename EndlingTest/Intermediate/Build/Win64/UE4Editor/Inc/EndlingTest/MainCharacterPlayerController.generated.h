// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ENDLINGTEST_MainCharacterPlayerController_generated_h
#error "MainCharacterPlayerController.generated.h already included, missing '#pragma once' in MainCharacterPlayerController.h"
#endif
#define ENDLINGTEST_MainCharacterPlayerController_generated_h

#define EndlingTest_Source_EndlingTest_MainCharacterPlayerController_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execMovementInput) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_AxisValue); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->MovementInput(Z_Param_AxisValue); \
		P_NATIVE_END; \
	}


#define EndlingTest_Source_EndlingTest_MainCharacterPlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execMovementInput) \
	{ \
		P_GET_PROPERTY(UFloatProperty,Z_Param_AxisValue); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->MovementInput(Z_Param_AxisValue); \
		P_NATIVE_END; \
	}


#define EndlingTest_Source_EndlingTest_MainCharacterPlayerController_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMainCharacterPlayerController(); \
	friend struct Z_Construct_UClass_AMainCharacterPlayerController_Statics; \
public: \
	DECLARE_CLASS(AMainCharacterPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlingTest"), NO_API) \
	DECLARE_SERIALIZER(AMainCharacterPlayerController)


#define EndlingTest_Source_EndlingTest_MainCharacterPlayerController_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAMainCharacterPlayerController(); \
	friend struct Z_Construct_UClass_AMainCharacterPlayerController_Statics; \
public: \
	DECLARE_CLASS(AMainCharacterPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/EndlingTest"), NO_API) \
	DECLARE_SERIALIZER(AMainCharacterPlayerController)


#define EndlingTest_Source_EndlingTest_MainCharacterPlayerController_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMainCharacterPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMainCharacterPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMainCharacterPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMainCharacterPlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMainCharacterPlayerController(AMainCharacterPlayerController&&); \
	NO_API AMainCharacterPlayerController(const AMainCharacterPlayerController&); \
public:


#define EndlingTest_Source_EndlingTest_MainCharacterPlayerController_h_12_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMainCharacterPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMainCharacterPlayerController(AMainCharacterPlayerController&&); \
	NO_API AMainCharacterPlayerController(const AMainCharacterPlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMainCharacterPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMainCharacterPlayerController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMainCharacterPlayerController)


#define EndlingTest_Source_EndlingTest_MainCharacterPlayerController_h_12_PRIVATE_PROPERTY_OFFSET
#define EndlingTest_Source_EndlingTest_MainCharacterPlayerController_h_9_PROLOG
#define EndlingTest_Source_EndlingTest_MainCharacterPlayerController_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlingTest_Source_EndlingTest_MainCharacterPlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	EndlingTest_Source_EndlingTest_MainCharacterPlayerController_h_12_RPC_WRAPPERS \
	EndlingTest_Source_EndlingTest_MainCharacterPlayerController_h_12_INCLASS \
	EndlingTest_Source_EndlingTest_MainCharacterPlayerController_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define EndlingTest_Source_EndlingTest_MainCharacterPlayerController_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	EndlingTest_Source_EndlingTest_MainCharacterPlayerController_h_12_PRIVATE_PROPERTY_OFFSET \
	EndlingTest_Source_EndlingTest_MainCharacterPlayerController_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	EndlingTest_Source_EndlingTest_MainCharacterPlayerController_h_12_INCLASS_NO_PURE_DECLS \
	EndlingTest_Source_EndlingTest_MainCharacterPlayerController_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID EndlingTest_Source_EndlingTest_MainCharacterPlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
