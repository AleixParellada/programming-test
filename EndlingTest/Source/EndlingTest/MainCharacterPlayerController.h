#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MainCharacterPawn.h"
#include "Engine/World.h"
#include "MainCharacterPlayerController.generated.h"

UCLASS()
class ENDLINGTEST_API AMainCharacterPlayerController : public APlayerController
{
	GENERATED_BODY()

	UFUNCTION(BlueprintCallable, Category = "CppFunctionCall")
	void MovementInput(float AxisValue);

protected:
	virtual void BeginPlay() override;

private:
	AMainCharacterPawn* MainCharacterPawn = nullptr;
	AMainCharacterPawn* GetMainCharacterPawn() const;
};
