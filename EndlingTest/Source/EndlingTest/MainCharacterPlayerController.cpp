#include "MainCharacterPlayerController.h"

void AMainCharacterPlayerController::BeginPlay()
{
	MainCharacterPawn = GetMainCharacterPawn();
}

void AMainCharacterPlayerController::MovementInput(float AxisValue)
{
	if (!MainCharacterPawn)
	{
		UE_LOG(LogTemp, Error, TEXT("MainCharacterPlayerController isn't possessing AMainCharacterPawn"));
		return;
	}
	else if (AxisValue != 0)
		AxisValue = AxisValue > 0 ? 1 : -1;
	MainCharacterPawn->MovementInputHandler(AxisValue);
}

AMainCharacterPawn * AMainCharacterPlayerController::GetMainCharacterPawn() const
{
	return Cast<AMainCharacterPawn>(GetPawn());
}
