#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "RabbitPawn.generated.h"

UCLASS()
class ENDLINGTEST_API ARabbitPawn : public APawn
{
	GENERATED_BODY()

public:
	ARabbitPawn();
	UPROPERTY(Category = "CppVariable", EditAnywhere, BlueprintReadWrite)
	bool HasOverlappedCharacter = false;
};
