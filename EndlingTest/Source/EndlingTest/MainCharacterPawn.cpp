#include "MainCharacterPawn.h"

AMainCharacterPawn::AMainCharacterPawn()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AMainCharacterPawn::BeginPlay()
{
	Super::BeginPlay();
}

void AMainCharacterPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	UpdatePawnPosition(DeltaTime);
}

void AMainCharacterPawn::MovementInputHandler(float AxisValue)
{
	if (HasDirectionChanged)
		return;
	else if (HasOverlappedRabbit)
		return;
	else if (AxisValue != 0)
	{
		HasDirectionChanged = PrevAxisValue != AxisValue;
		PrevAxisValue = AxisValue;
		FRotator CharacterRotation = AxisValue == 1 ? FRotator(0, 0, 0) : FRotator(0, 180 * AxisValue, 0);
		SetActorRotation(CharacterRotation);
	}
	AddMovementInput(GetActorForwardVector(), AxisValue * MovementSpeed);
}

void AMainCharacterPawn::UpdatePawnPosition(float DeltaTime)
{
	AddActorLocalOffset(ConsumeMovementInputVector() * DeltaTime);
}

void AMainCharacterPawn::SetupPlayerInputComponent(UInputComponent * InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
}
