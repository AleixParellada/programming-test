// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EndlingTestGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ENDLINGTEST_API AEndlingTestGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
