#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "MainCharacterPawn.generated.h"

UCLASS()
class ENDLINGTEST_API AMainCharacterPawn : public APawn
{
	GENERATED_BODY()

public:
	AMainCharacterPawn();
	UPROPERTY(Category = "CppVariable", EditAnywhere, BlueprintReadWrite)
	float MovementSpeed = 100.f;
	UPROPERTY(Category = "CppVariable", EditAnywhere, BlueprintReadWrite)
	bool HasDirectionChanged = false;
	UPROPERTY(Category = "CppVariable", EditAnywhere, BlueprintReadWrite)
	bool HasOverlappedRabbit = false;
	virtual void Tick(float DeltaTime) override;
	void MovementInputHandler(float AxisValue);

protected:
	virtual void BeginPlay() override;

private:
	float PrevAxisValue = 1;
	void UpdatePawnPosition(float DeltaTime);
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
};
