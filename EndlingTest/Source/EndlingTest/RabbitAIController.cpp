#include "RabbitAIController.h"

void ARabbitAIController::BeginPlay()
{
	Super::BeginPlay();
	ControlledPawn = GetControlledPawn();
	CharacterPawn = GetCharacterPawn();
}

void ARabbitAIController::OnPerceptionUpdated(TArray<AActor*> UpdatedActors)
{
	bool HasActorBeenPerceived = false;
	if (ControlledPawn->HasOverlappedCharacter)
		return;
	else if (!HasPerceivedCharacter(UpdatedActors))
		return;
	else if (!BlackboardComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("There is no Blackboard component attached to AI Controller."));
		return;
	}
	BlackboardComponent->SetValueAsBool(FName("HasSeenEnemy"), true);
}

bool ARabbitAIController::HasPerceivedCharacter(TArray<AActor*> UpdatedActors)
{
	for (AActor* Actor : UpdatedActors)
	{
		if (Actor == CharacterPawn)
			return true;
	}
	return false;
}

void ARabbitAIController::OnFleeReceiveExecute()
{
	if (!ControlledPawn)
	{
		UE_LOG(LogTemp, Error, TEXT("AI Controller hasn't any Controlled Pawn."));
		return;
	}
	else if (!CharacterPawn)
	{
		UE_LOG(LogTemp, Error, TEXT("AI Controller hasn't find the Main Character."));
		return;
	}
	FVector ForwardVector = ControlledPawn->GetActorForwardVector();
	if (ForwardVector == CharacterPawn->GetActorForwardVector())
		return;
	FRotator Rotation = ForwardVector == FVector(1, 0, 0) ? FRotator(0, 180, 0) : FRotator(0, 0, 0);
	ControlledPawn->SetActorRotation(Rotation);
}

void ARabbitAIController::OnFleeReceiveTick(float DeltaTime)
{
	if (ControlledPawn->HasOverlappedCharacter)
		return;
	ControlledPawn->AddMovementInput(ControlledPawn->GetActorForwardVector(), FleeSpeed * DeltaTime);
}

ARabbitPawn * ARabbitAIController::GetControlledPawn() const
{
	APawn* Pawn = GetPawn();
	return Cast<ARabbitPawn>(Pawn);
}

AMainCharacterPawn * ARabbitAIController::GetCharacterPawn() const
{
	APawn* Pawn = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
	return Cast<AMainCharacterPawn>(Pawn);
}

void ARabbitAIController::OnOverlapBegin()
{
	ControlledPawn->SetActorLocation(CharacterPawn->GetActorLocation());
	BlackboardComponent->SetValueAsBool(FName("HasBeenBitten"), true);
	BlackboardComponent->SetValueAsBool(FName("HasSeenEnemy"), false);
}
void ARabbitAIController::OnPawnDied()
{
	TArray<USkeletalMeshComponent*> Components;
	CharacterPawn->GetComponents<USkeletalMeshComponent>(Components);
	for (int32 i = 0; i < Components.Num(); i++)
	{
		USkeletalMeshComponent* SkeletalMeshComponent = Components[i];
		FVector FinalLocation = SkeletalMeshComponent->GetBoneLocation("Head_M");
		ControlledPawn->SetActorLocation(FVector(FinalLocation.X, 0.f, 1.f));
		break;
	}

	TArray<USkeletalMeshComponent*> RabbitComponents;
	ControlledPawn->GetComponents<USkeletalMeshComponent>(RabbitComponents);
	for (int32 i = 0; i < RabbitComponents.Num(); i++)
	{
		USkeletalMeshComponent* SkeletalMeshComponent = RabbitComponents[i];
		SkeletalMeshComponent->SetSimulatePhysics(true);
		break;
	}


}
