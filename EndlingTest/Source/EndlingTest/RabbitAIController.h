#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "RabbitPawn.h"
#include "MainCharacterPawn.h"
#include "Engine/World.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BlackboardComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/Components/SkeletalMeshComponent.h"
#include "RabbitAIController.generated.h"

UCLASS()
class ENDLINGTEST_API ARabbitAIController : public AAIController
{
	GENERATED_BODY()

	UFUNCTION(BlueprintCallable, Category = "CppFunctionCall")
	void OnPerceptionUpdated(TArray<AActor*> UpdatedActors);
	UFUNCTION(BlueprintCallable, Category = "CppFunctionCall")
	void OnOverlapBegin();
	UFUNCTION(BlueprintCallable, Category = "CppFunctionCall")
	void OnFleeReceiveExecute();
	UFUNCTION(BlueprintCallable, Category = "CppFunctionCall")
	void OnFleeReceiveTick(float DeltaTime);
	UFUNCTION(BlueprintCallable, Category = "CppFunctionCall")
	void OnPawnDied();

public:
	UPROPERTY(Category = "CppVariable", EditAnywhere, BlueprintReadWrite)
	UBlackboardComponent* BlackboardComponent = nullptr;
	UPROPERTY(Category = "CppVariable", EditAnywhere, BlueprintReadWrite)
	float FleeSpeed = 15.f;

protected:
	virtual void BeginPlay() override;

private:
	ARabbitPawn* ControlledPawn = nullptr;
	AMainCharacterPawn* CharacterPawn = nullptr;
	bool HasPerceivedCharacter(TArray<AActor*> UpdatedActors);
	ARabbitPawn* GetControlledPawn() const;
	AMainCharacterPawn* GetCharacterPawn() const;
};
